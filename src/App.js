import React from "react";
import { GajiProvider } from "./contexts/GajiContext";
import Home from "./pages/Home";

const App = () => {
  return (
    <GajiProvider>
      <Home />
    </GajiProvider>
  );
};

export default App;
