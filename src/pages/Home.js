import { PDFViewer } from "@react-pdf/renderer";
import React from "react";
import GajiPdf from "../cetak/GajiPdf";
import DataKaryawan from "../components/DataKaryawan";
import Footer from "../components/Footer";
import Gaji from "../components/Gaji";
import SelectKaryawan from "../components/SelectKaryawan";
import { useGaji } from "../contexts/GajiContext";

const Home = () => {
  const { selectedKaryawan, printNow } = useGaji();
  return (
    <div className="overflow-x-hidden">
      <main className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8 md:mb-12 p-4">
        <div className="text-center bg-gradient-to-r from-green-500 to-green-900 py-2">
          <h1 className="text-4xl font-extrabold text-white tracking-wide mt-7">
            Hitung Gaji Pegawai
          </h1>
          <p className="text-lg font-semibold text-white">
            PPS Ibnu Mas'ud Kampar
          </p>
          <div className="flex justify-between pr-4 pl-4 text-gray-300">
            <a href="#" className="hover:text-white">
              @programmercintasunnah
            </a>
            <p>created at 23/03/2023</p>
          </div>
        </div>
        <div className="mt-10">
          <SelectKaryawan />
        </div>
        <div className={selectedKaryawan ? "flex flex-wrap" : ""}>
          {selectedKaryawan && (
            <div className="w-full md:w-1/2 p-10">
              <DataKaryawan />
            </div>
          )}
          <div
            className={
              selectedKaryawan ? "w-full md:w-1/2 p-10" : "mt-10 mb-10"
            }
          >
            <Gaji />
          </div>
        </div>
      </main>
      {/* )} */}
      <Footer />
    </div>
  );
};

export default Home;
