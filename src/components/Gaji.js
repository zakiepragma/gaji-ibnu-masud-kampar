import React, { useEffect, useState } from "react";
import CurrencyInput from "react-currency-input-field";
import { useGaji } from "../contexts/GajiContext";

const Gaji = () => {
  const {
    hitungTunjanganJamMengajar,
    hitungTunjanganMakan,
    jumlahHariAktif,
    setJumlahHariAktif,
    jumlahPiket,
    setJumlahPiket,
    jumlahBimbinganSiang,
    setJumlahBimbinganSiang,
    jumlahBimbinganMalam,
    setJumlahBimbinganMalam,
    jumlahHariTidakHadir,
    setJumlahHariTidakHadir,
    pinjaman,
    setPinjaman,
  } = useGaji();

  const [jumlahJamMengajar, setJumlahJamMengajar] = useState(0);

  const handleChangeJamMengajar = (e) => {
    const rawValue = e.target.value;
    let value = parseInt(rawValue);

    if (isNaN(value)) {
      value = "";
    }

    setJumlahJamMengajar(value);
    hitungTunjanganJamMengajar(value);
  };

  const handleChangeHariAktif = (e) => {
    const rawValue = e.target.value;
    let value = parseInt(rawValue);
    if (isNaN(value)) {
      value = "";
    }

    setJumlahHariAktif(value);
  };

  const handleChangeJumlahPiket = (e) => {
    const rawValue = e.target.value;
    let value = parseInt(rawValue);
    if (isNaN(value)) {
      value = "";
    }

    setJumlahPiket(value);
  };

  const handleChangeBimbinganSiang = (e) => {
    const rawValue = e.target.value;
    let value = parseInt(rawValue);
    if (isNaN(value)) {
      value = "";
    }

    setJumlahBimbinganSiang(value);
  };

  const handleChangeBimbinganMalam = (e) => {
    const rawValue = e.target.value;
    let value = parseInt(rawValue);
    if (isNaN(value)) {
      value = "";
    }

    setJumlahBimbinganMalam(value);
  };

  const handleChangeJumlahTidakHadir = (e) => {
    const rawValue = e.target.value;
    let value = parseInt(rawValue);
    if (isNaN(value)) {
      value = "";
    }

    setJumlahHariTidakHadir(value);
  };

  const handleChangePinjaman = (newValue) => {
    const rawValue = newValue;
    let value = parseInt(rawValue);
    if (isNaN(value)) {
      value = "";
    }

    setPinjaman(value);
  };

  useEffect(() => {
    hitungTunjanganMakan(jumlahHariAktif);
  });

  return (
    <div className="w-full">
      <div className="flex flex-wrap -mx-2">
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Jumlah Hari Aktif:
          </label>
          <input
            type="number"
            className="w-full border rounded px-3 py-2"
            value={jumlahHariAktif}
            onChange={handleChangeHariAktif}
          />
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Jumlah Jam Mengajar:
          </label>
          <input
            type="number"
            className="w-full border rounded px-3 py-2"
            value={jumlahJamMengajar}
            onChange={handleChangeJamMengajar}
          />
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Jumlah Hari Piket:
          </label>
          <input
            type="number"
            className="w-full border rounded px-3 py-2"
            value={jumlahPiket}
            onChange={handleChangeJumlahPiket}
          />
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Jumlah Bimbingan Siang:
          </label>
          <input
            type="number"
            className="w-full border rounded px-3 py-2"
            value={jumlahBimbinganSiang}
            onChange={handleChangeBimbinganSiang}
          />
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Jumlah Bimbingan Malam:
          </label>
          <input
            type="number"
            className="w-full border rounded px-3 py-2"
            value={jumlahBimbinganMalam}
            onChange={handleChangeBimbinganMalam}
          />
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Jumlah Hari Tidak Hadir:
          </label>
          <input
            type="number"
            className="w-full border rounded px-3 py-2"
            value={jumlahHariTidakHadir}
            onChange={handleChangeJumlahTidakHadir}
          />
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2 px-2 mb-4">
          <label className="block mb-2 font-bold text-gray-700">
            Pinjaman:
          </label>
          <CurrencyInput
            prefix={"Rp "}
            className="w-full border rounded px-3 py-2"
            value={pinjaman}
            onValueChange={handleChangePinjaman}
          />
        </div>
      </div>
    </div>
  );
};

export default Gaji;
