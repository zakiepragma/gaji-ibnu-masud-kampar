import React from "react";
import { useGaji } from "../contexts/GajiContext";

const SelectKaryawan = () => {
  const { semuaKaryawan, setIdKaryawan } = useGaji();

  const handleChangeKaryawan = (event) => {
    const id = parseInt(event.target.value);
    setIdKaryawan(id);
  };

  return (
    <div className="flex items-center">
      <select
        onChange={handleChangeKaryawan}
        className="w-full border rounded-md p-2 mr-4"
      >
        <option value="">Select Karyawan</option>
        {semuaKaryawan.map((karyawan) => (
          <option key={karyawan.id} value={karyawan.id}>
            {karyawan.nama}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectKaryawan;
