import React from "react";
import { useGaji } from "../contexts/GajiContext";

const DataKaryawan = () => {
  const { selectedKaryawan, calculateTimeSinceStart } = useGaji();

  return (
    <div className="table w-full">
      <ul className="table-row-group">
        <li className="table-row">
          <div className="table-cell p-2 font-bold">Nama</div>
          <div className="table-cell p-2">{selectedKaryawan.nama}</div>
        </li>
        <li className="table-row">
          <div className="table-cell p-2 font-bold">Pendidikan</div>
          <div className="table-cell p-2">{selectedKaryawan.pendidikan}</div>
        </li>
        <li className="table-row">
          <div className="table-cell p-2 font-bold">Jabatan</div>
          <div className="table-cell p-2">
            {selectedKaryawan.jabatan != ""
              ? selectedKaryawan.jabatan.join(", ")
              : "-"}
          </div>
        </li>
        <li className="table-row">
          <div className="table-cell p-2 font-bold">Mulai Kerja</div>
          <div className="table-cell p-2">
            {selectedKaryawan.mulaiKerja} (
            {calculateTimeSinceStart(selectedKaryawan.mulaiKerja) + " tahun"})
          </div>
        </li>
        <li className="table-row">
          <div className="table-cell p-2 font-bold">Apakah Walikelas</div>
          <div className="table-cell p-2">
            {selectedKaryawan.waliKelas ? "Ya" : "Tidak"}
          </div>
        </li>
        <li className="table-row">
          <div className="table-cell p-2 font-bold">Jarak Domisili</div>
          <div className="table-cell p-2">
            {selectedKaryawan.jarakDomisili} Km
          </div>
        </li>
         <li className="table-row">
          <div className="table-cell p-2 font-bold">Tunjangan Yayasan</div>
          <div className="table-cell p-2">
            Rp. {selectedKaryawan.tunjanganYayasan? "200.000": "0"}
          </div>
        </li>
      </ul>
    </div>
  );
};

export default DataKaryawan;
