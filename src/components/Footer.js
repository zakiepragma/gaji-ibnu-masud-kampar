import React from "react";
import CurrencyFormat from "react-currency-input-field";
import { useGaji } from "../contexts/GajiContext";
import GajiPdf from "../cetak/GajiPdf";
import { PDFDownloadLink } from "@react-pdf/renderer";

const Footer = () => {
  const {
    setPrintNow,
    selectedKaryawan,
    gajiPokok,
    pinjaman,
    totalGaji,
    tunjanganMasaKerja,
    tunjanganJamMengajar,
    tunjanganMakan,
    tunjanganTransportasi,
    tunjanganKesehatan,
    tunjanganWalikelas,
    tunjanganJabatan,
    tunjanganPiket,
    tunjanganBimbinganSiang,
    tunjanganBimbinganMalam,
    tunjanganYayasan,
    penguranganTunjanganKetidakhadiran,
  } = useGaji();

  const fileName = selectedKaryawan
    ? `Gaji - ${selectedKaryawan.nama}.pdf`
    : "gaji.pdf";

  return (
    <footer className="md:fixed bottom-0 w-full bg-gray-200 p-4">
      <div className="flex flex-wrap md:flex-nowrap justify-between items-center">
        <div className="w-full md:w-auto mb-4 md:mb-0 md:mr-4">
          <div className="bg-gray-200 rounded-lg">
            <h3 className="font-bold text-gray-700 mb-2">Total Gaji:</h3>
            <div className="text-3xl font-bold text-gray-900">
              <CurrencyFormat
                value={totalGaji}
                // displayType={"text"}
                // thousandSeparator={true}
                prefix={"Rp "}
                className="p-2"
              />
            </div>
          </div>
        </div>
        {/* <button
          className="bg-orange-500 hover:bg-orange-600 text-white rounded-md p-2 md:ml-auto"
          onClick={(e) => setPrintNow(true)}
        >
          Preview PDF
        </button> */}
        {selectedKaryawan && (
          <PDFDownloadLink
            document={
              <GajiPdf
                karyawan={selectedKaryawan}
                gajiPokok={gajiPokok}
                pinjaman={pinjaman}
                totalGaji={totalGaji}
                tunjanganMasaKerja={tunjanganMasaKerja}
                tunjanganJamMengajar={tunjanganJamMengajar}
                tunjanganMakan={tunjanganMakan}
                tunjanganTransportasi={tunjanganTransportasi}
                tunjanganKesehatan={tunjanganKesehatan}
                tunjanganWalikelas={tunjanganWalikelas}
                tunjanganJabatan={tunjanganJabatan}
                tunjanganPiket={tunjanganPiket}
                tunjanganBimbinganSiang={tunjanganBimbinganSiang}
                tunjanganBimbinganMalam={tunjanganBimbinganMalam}
                tunjanganYayasan={tunjanganYayasan}
                penguranganTunjanganKetidakhadiran={
                  penguranganTunjanganKetidakhadiran
                }
              />
            }
            className="bg-green-500 hover:bg-green-600 text-white rounded-md p-2 md:ml-auto"
            fileName={fileName}
          >
            {({ blob, url, loading, error }) =>
              loading ? "Loading document..." : "Print PDF"
            }
          </PDFDownloadLink>
        )}
      </div>
    </footer>
  );
};

export default Footer;
