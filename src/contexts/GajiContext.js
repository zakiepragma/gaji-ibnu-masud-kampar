import { createContext, useContext, useEffect, useState } from "react";
import Jabatan from "../data/Jabatan";
import Pendidikan from "../data/Pendidikan";
import SemuaKaryawan from "../data/SemuaKaryawan";

const GajiContext = createContext();

export const useGaji = () => useContext(GajiContext);

export const GajiProvider = ({ children }) => {
  const [semuaKaryawan, setSemuaKaryawan] = useState(SemuaKaryawan);
  const [selectedKaryawan, setSelectedKaryawan] = useState(null);
  const [pendidikan, setPendidikan] = useState(Pendidikan);
  const [jabatan, setJabatan] = useState(Jabatan);

  const [jumlahHariAktif, setJumlahHariAktif] = useState(26);
  const [jumlahPiket, setJumlahPiket] = useState(0);
  const [jumlahBimbinganSiang, setJumlahBimbinganSiang] = useState(0);
  const [jumlahBimbinganMalam, setJumlahBimbinganMalam] = useState(0);
  const [jumlahHariTidakHadir, setJumlahHariTidakHadir] = useState(0);
  const [pinjaman, setPinjaman] = useState(0);

  const [gajiPokok, setGajiPokok] = useState(0);
  const [tunjanganMasaKerja, setTunjanganMasaKerja] = useState(0);
  const [tunjanganJamMengajar, setTunjanganJamMengajar] = useState(0);
  const [tunjanganMakan, setTunjanganMakan] = useState(0);
  const [tunjanganTransportasi, setTunjanganTransportasi] = useState(0);
  const [tunjanganKesehatan, setTunjanganKesehatan] = useState(50000);
  const [tunjanganWalikelas, setTunjanganWalikelas] = useState(0);
  const [tunjanganJabatan, setTunjanganJabatan] = useState(0);
  const [tunjanganPiket, setTunjanganPiket] = useState(0);
  const [tunjanganBimbinganSiang, setTunjanganBimbinganSiang] = useState(0);
  const [tunjanganBimbinganMalam, setTunjanganBimbinganMalam] = useState(0);
  const [
    penguranganTunjanganKetidakhadiran,
    setPenguranganTunjanganKetidakhadiran,
  ] = useState(0);

  const [tunjanganYayasan, setTunjanganYayasan] = useState(0);

  const [idKaryawan, setIdKaryawan] = useState("");

  const [printNow, setPrintNow] = useState(false);
  const [mudir, setMudir] = useState(false);

  const [totalGaji, setTotalGaji] = useState(0);

  const getKaryawanById = (id) => {
    const karyawan = semuaKaryawan.find((karyawan) => karyawan.id === id);
    setSelectedKaryawan(karyawan);
    hitungTunjanganPendidikan(karyawan.pendidikan);
    setTunjanganMasaKerja(calculateTimeSinceStart(karyawan.mulaiKerja) * 50000);
    hitungTunjanganTransportasi(karyawan.jarakDomisili, jumlahHariAktif);
    hitungTunjanganWaliKelas(karyawan.waliKelas);
    hitungTunjanganJabatan(karyawan.jabatan);
    hitungPenguranganKetidakhadiran(
      karyawan.jarakDomisili,
      jumlahHariTidakHadir
    );

    setTunjanganYayasan(karyawan.tunjanganYayasan?200000:0)
  };

  useEffect(() => {
    if (idKaryawan) {
      getKaryawanById(idKaryawan);
    }
  });

  const hitungTunjanganWaliKelas = (walikelas) => {
    if (walikelas) {
      setTunjanganWalikelas(50000);
    } else {
      setTunjanganWalikelas(0);
    }
  };

  const hitungTunjanganPendidikan = (pendidikanKaryawan) => {
    pendidikan.map((p) => {
      if (p.nama === pendidikanKaryawan) {
        setGajiPokok(p.tunjangan);
      }
    });
  };

  const hitungTunjanganJabatan = (jabatanKaryawan) => {
    let totalTunjangan = 0;
    let namaJabatan = "";
    jabatanKaryawan.map((jKaryawan) => {
      jabatan.map((j) => {
        if (j.nama === jKaryawan) {
          totalTunjangan += j.tunjangan;
          namaJabatan = j.nama;
        }
      });
    });
    if (namaJabatan === "Pimpinan/Kepala Pesantren") {
      setMudir(true);
    } else {
      setMudir(false);
    }
    setTunjanganJabatan(totalTunjangan);
  };

  const hitungTunjanganTransportasi = (jarakDomisili, hariKerja) => {
    if (jarakDomisili > 15) {
      setTunjanganTransportasi(hariKerja * 7000);
    } else if (jarakDomisili > 10) {
      setTunjanganTransportasi(hariKerja * 5000);
    } else if (jarakDomisili > 5) {
      setTunjanganTransportasi(hariKerja * 3500);
    } else {
      setTunjanganTransportasi(hariKerja * 2500);
    }
  };

  const hitungTunjanganMakan = (hariMengajar) => {
    setTunjanganMakan(hariMengajar * 8000);
  };

  const hitungPenguranganKetidakhadiran = (jarakDomisili, hariTidakKerja) => {
    let totalPengurangan = 0;

    if (jarakDomisili > 15) {
      totalPengurangan += hariTidakKerja * 7000;
    } else if (jarakDomisili > 10) {
      totalPengurangan += hariTidakKerja * 5000;
    } else if (jarakDomisili > 5) {
      totalPengurangan += hariTidakKerja * 3500;
    } else {
      totalPengurangan += hariTidakKerja * 2500;
    }

    totalPengurangan += hariTidakKerja * 8000;

    setPenguranganTunjanganKetidakhadiran(totalPengurangan);
  };

  const hitungTunjanganJamMengajar = (jamMengajar) => {
    setTunjanganJamMengajar(jamMengajar * 12500);
  };

  const calculateTimeSinceStart = (dateString) => {
    const [day, month, year] = dateString.split("-");
    const startDate = new Date(`${year}-${month}-${day}`);
    const currentDate = new Date();
    const timeDiff = currentDate.getTime() - startDate.getTime();
    const yearsSinceStart = Math.floor(
      timeDiff / (1000 * 60 * 60 * 24 * 365.25)
    );
    const monthsSinceStart = Math.floor(
      (timeDiff % (1000 * 60 * 60 * 24 * 365.25)) /
        (1000 * 60 * 60 * 24 * (365.25 / 12))
    );
    const daysSinceStart = Math.floor(
      (timeDiff % (1000 * 60 * 60 * 24 * (365.25 / 12))) / (1000 * 60 * 60 * 24)
    );
    console.log(yearsSinceStart);
    return yearsSinceStart;
  };

  useEffect(() => {
    setTunjanganPiket(jumlahPiket * 18500);
    setTunjanganBimbinganSiang(jumlahBimbinganSiang * 12500);
    setTunjanganBimbinganMalam(jumlahBimbinganMalam * 32000);
  });

  useEffect(() => {
    if (mudir) {
      setTotalGaji(4000000 - pinjaman);
    } else {
      setTotalGaji(
        gajiPokok +
          tunjanganMasaKerja +
          tunjanganJamMengajar +
          tunjanganMakan +
          tunjanganTransportasi +
          tunjanganKesehatan +
          tunjanganWalikelas +
          tunjanganJabatan +
          tunjanganPiket +
          tunjanganBimbinganSiang +
          tunjanganBimbinganMalam +
          tunjanganYayasan -
          penguranganTunjanganKetidakhadiran -
          pinjaman
      );
    }
  });

  return (
    <GajiContext.Provider
      value={{
        semuaKaryawan,
        selectedKaryawan,
        calculateTimeSinceStart,
        hitungTunjanganJamMengajar,
        hitungTunjanganMakan,
        jumlahHariAktif,
        setJumlahHariAktif,
        setIdKaryawan,
        jumlahPiket,
        setJumlahPiket,
        jumlahBimbinganSiang,
        setJumlahBimbinganSiang,
        jumlahBimbinganMalam,
        setJumlahBimbinganMalam,
        jumlahHariTidakHadir,
        setJumlahHariTidakHadir,
        setPinjaman,
        printNow,
        setPrintNow,
        gajiPokok,
        pinjaman,
        totalGaji,
        tunjanganMasaKerja,
        tunjanganJamMengajar,
        tunjanganMakan,
        tunjanganTransportasi,
        tunjanganKesehatan,
        tunjanganWalikelas,
        tunjanganJabatan,
        tunjanganPiket,
        tunjanganBimbinganSiang,
        tunjanganBimbinganMalam,
        tunjanganYayasan,
        penguranganTunjanganKetidakhadiran,
      }}
    >
      {children}
    </GajiContext.Provider>
  );
};
