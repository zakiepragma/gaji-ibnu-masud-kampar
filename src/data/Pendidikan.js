const Pendidikan = [
  {
    id: 1,
    nama: "SD",
    tunjangan: 600000,
  },
  {
    id: 2,
    nama: "SLTP",
    tunjangan: 700000,
  },
  {
    id: 3,
    nama: "SLTA",
    tunjangan: 800000,
  },
  {
    id: 4,
    nama: "D1",
    tunjangan: 900000,
  },
  {
    id: 5,
    nama: "D2",
    tunjangan: 900000,
  },
  {
    id: 6,
    nama: "D3",
    tunjangan: 900000,
  },
  {
    id: 7,
    nama: "S1",
    tunjangan: 1050000,
  },
];

export default Pendidikan;
