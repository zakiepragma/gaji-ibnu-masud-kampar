const SemuaKaryawan = [
  {
    id: 1,
    nama: "Muazia Ulhaq, Lc., S.Pd.I",
    pendidikan: "S1",
    mulaiKerja: "12-01-2022",
    jarakDomisili: 13, // dalam km
    jabatan: ["Pimpinan/Kepala Pesantren"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 2,
    nama: "Umar Hadi",
    pendidikan: "S1",
    mulaiKerja: "11-07-2011",
    jarakDomisili: 0, // dalam km
    jabatan: [
      // "Waka Bidang Kesantrian Putra",
      // "Koordinator Program Pendidikan Khusus",
      "Waka Bidang Tahfidz Al-Qur'an Putra",
    ],
    waliKelas: true,
    tunjanganYayasan:true,
  },
  {
    id: 3,
    nama: "Indah Rahma",
    pendidikan: "SLTA",
    mulaiKerja: "11-07-2011",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 4,
    nama: "Rubaina, B.A",
    pendidikan: "D3",
    mulaiKerja: "11-07-2011",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Tahfidz Al-Qur'an Putri"],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 5,
    nama: "Agusnizar, A.Ma.Pd.SD",
    pendidikan: "D2",
    mulaiKerja: "02-10-2011",
    jarakDomisili: 10, // dalam km
    jabatan: ["Waka Bidang Kurikulum"],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 6,
    nama: "Erwin Syah",
    pendidikan: "SLTA",
    mulaiKerja: "09-07-2012",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Pemondokan Putra"],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 7,
    nama: "Hendri Yana, S.Pd.I",
    pendidikan: "S1",
    mulaiKerja: "09-07-2012",
    jarakDomisili: 11, // dalam km
    jabatan: [],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 8,
    nama: "Aci Suciati",
    pendidikan: "SLTA",
    mulaiKerja: "09-07-2013",
    jarakDomisili: 11, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 9,
    nama: "Mulyadi, S.Pd.I",
    pendidikan: "S1",
    mulaiKerja: "20-10-2013",
    jarakDomisili: 11, // dalam km
    jabatan: ["Bendahara (Pengelola SPP)"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 10,
    nama: "Irfan Afendi, S.Pd.I",
    pendidikan: "S1",
    mulaiKerja: "07-07-2014",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Humas"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 11,
    nama: "Muliadi",
    pendidikan: "SLTA",
    mulaiKerja: "07-07-2014",
    jarakDomisili: 0, // dalam km
    jabatan: ["Jaga Pesantren"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 12,
    nama: "Rusailah Najiah",
    pendidikan: "SLTA",
    mulaiKerja: "11-08-2014",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Kesantrian Putri"],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 13,
    nama: "Amrullah",
    pendidikan: "S1",
    mulaiKerja: "06-01-2015",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 14,
    nama: "Hamdi, S.P",
    pendidikan: "S1",
    mulaiKerja: "06-07-2015",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Sarana Prasarana"],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 15,
    nama: "Muhammad Ilham",
    pendidikan: "S1",
    mulaiKerja: "06-07-2015",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 16,
    nama: "Ii Afri Ningsih",
    pendidikan: "D2",
    mulaiKerja: "06-07-2015",
    jarakDomisili: 0, // dalam km
    jabatan: [
      "Waka Bidang Pemondokan Putri",
      // "Koordinator Program Pendidikan Khusus",
    ],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 17,
    nama: "Adol Bastian",
    pendidikan: "SLTA",
    mulaiKerja: "06-07-2015",
    jarakDomisili: 0, // dalam km
    jabatan: ["Jaga Pesantren"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 18,
    nama: "Riko Candra Puta",
    pendidikan: "S1",
    mulaiKerja: "01-09-2015",
    jarakDomisili: 11, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 19,
    nama: "Nurlizar S.Si",
    pendidikan: "S1",
    mulaiKerja: "19-09-2015",
    jarakDomisili: 12, // dalam km
    jabatan: ["Tenaga ADM"],
    waliKelas: false,
    tunjanganYayasan:true,
  },
  {
    id: 20,
    nama: "Misbah, A.Ma.Pd.SD",
    pendidikan: "D2",
    mulaiKerja: "11-07-2016",
    jarakDomisili: 11, // dalam km
    jabatan: [],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 21,
    nama: "Muhammad Fajri",
    pendidikan: "S1",
    mulaiKerja: "11-07-2016",
    jarakDomisili: 10, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 22,
    nama: "Dawir Ruci, S.E",
    pendidikan: "S1",
    mulaiKerja: "11-07-2016",
    jarakDomisili: 10, // dalam km
    jabatan: [],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 23,
    nama: "Ainur Rofi Gufron",
    pendidikan: "S1",
    mulaiKerja: "01-10-2017",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Kesantrian Putra"],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 24,
    nama: "Al Mar'atus Sholiha",
    pendidikan: "SLTA",
    mulaiKerja: "02-07-2018",
    jarakDomisili: 10, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 25,
    nama: "Nurhafizah",
    pendidikan: "SLTA",
    mulaiKerja: "02-07-2018",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 26,
    nama: "Hendrian Pratama, A.Md",
    pendidikan: "S1",
    mulaiKerja: "02-07-2018",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 27,
    nama: "Amira Mastura",
    pendidikan: "D2",
    mulaiKerja: "15-07-2019",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 28,
    nama: "Ade Hendra Wansyah",
    pendidikan: "SLTA",
    mulaiKerja: "06-01-2020",
    jarakDomisili: 6, // dalam km
    jabatan: ["Satpam Pesantren"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 29,
    nama: "Robi Hapiarto",
    pendidikan: "SLTA",
    mulaiKerja: "06-01-2020",
    jarakDomisili: 0, // dalam km
    jabatan: ["Satpam Pesantren"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 30,
    nama: "Roni Muliadi",
    pendidikan: "SLTA",
    mulaiKerja: "01-01-2021",
    jarakDomisili: 10, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 31,
    nama: "Ummi Kalsum",
    pendidikan: "D2",
    mulaiKerja: "12-07-2021",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },
  {
    id: 32,
    nama: "Nur Fajriati",
    pendidikan: "S1",
    mulaiKerja: "14-07-2022",
    jarakDomisili: 0, // dalam km
    jabatan: [],
    waliKelas: true,
    tunjanganYayasan:false,
  },

  //tk
  {
    id: 33,
    nama: "Rahma Melly, S.Pd",
    pendidikan: "S1",
    mulaiKerja: "06-07-2015",
    jarakDomisili: 6, // dalam km
    jabatan: [],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 34,
    nama: "Mesi Hayati",
    pendidikan: "SLTA",
    mulaiKerja: "11-07-2016",
    jarakDomisili: 0, // dalam km
    jabatan: ["Waka Bidang Pra Tahfizh (TK)"],
    waliKelas: false,
    tunjanganYayasan:false,
  },
  {
    id: 35,
    nama: "Yetni Ulfa, A.Ma",
    pendidikan: "D2",
    mulaiKerja: "24-10-2017",
    jarakDomisili: 1, // dalam km
    jabatan: [],
    waliKelas: false,
    tunjanganYayasan:false,
  },
];

export default SemuaKaryawan;
