const Jabatan = [
  {
    id: 1,
    nama: "Pimpinan/Kepala Pesantren",
    tunjangan: 4000000,
  },
  {
    id: 2,
    nama: "Waka Bidang Kurikulum",
    tunjangan: 350000,
  },
  {
    id: 3,
    nama: "Waka Bidang Kesantrian Putra",
    tunjangan: 350000,
  },
  {
    id: 4,
    nama: "Waka Bidang Kesantrian Putri",
    tunjangan: 350000,
  },
  {
    id: 5,
    nama: "Waka Bidang Pemondokan Putra",
    tunjangan: 350000,
  },
  {
    id: 6,
    nama: "Waka Bidang Pemondokan Putri",
    tunjangan: 350000,
  },
  {
    id: 7,
    nama: "Waka Bidang Sarana Prasarana",
    tunjangan: 350000,
  },
  {
    id: 8,
    nama: "Waka Bidang Humas",
    tunjangan: 350000,
  },
  {
    id: 9,
    nama: "Tenaga ADM",
    tunjangan: 350000,
  },
  {
    id: 10,
    nama: "Bendahara (Pengelola SPP)",
    tunjangan: 350000,
  },
  {
    id: 11,
    nama: "Jaga Pesantren",
    tunjangan: 250000,
  },
  {
    id: 12,
    nama: "Satpam Pesantren",
    tunjangan: 250000,
  },
  {
    id: 13,
    nama: "Koordinator Pustaka",
    tunjangan: 150000,
  },
  {
    id: 14,
    nama: "Waka Bidang Pra Tahfizh (TK)",
    tunjangan: 350000,
  },
  {
    id: 15,
    nama: "Koordinator Tahfizh",
    tunjangan: 350000,
  },
  {
    id: 16,
    nama: "Koordinator Program Pendidikan Khusus",
    tunjangan: 350000,
  },
  {
    id: 17,
    nama: "Waka Bidang Tahfidz Al-Qur'an Putra",
    tunjangan: 350000,
  },
  {
    id: 18,
    nama: "Waka Bidang Tahfidz Al-Qur'an Putri",
    tunjangan: 350000,
  },
];

export default Jabatan;
