import React from "react";
import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";

// Define styles
const styles = StyleSheet.create({
  page: {
    padding: 30,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 20,
    textAlign: "center",
  },
  row: {
    flexDirection: "row",
    marginBottom: 10,
  },
  label: {
    width: 150,
    fontWeight: "bold",
  },
  titikDua: {
    width: 10,
    fontWeight: "bold",
  },
  value: {
    flex: 1,
  },
  table: {
    borderWidth: 1,
    borderColor: "black",
    marginVertical: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
  tableRow: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "black",
    paddingVertical: 5,
  },
  tableHeader: {
    fontWeight: "bold",
    backgroundColor: "#ccc",
  },
  tableData: {
    flex: 1,
    textAlign: "center",
  },
  tableJudul: {
    flex: 1,
  },

  centeredRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },

  centeredLabel: {
    fontWeight: "bold",
    textAlign: "center",
  },

  centeredValue: {
    textAlign: "center",
  },

  signatureContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 35,
  },

});

// Function to get Bahasa Indonesia month name
const getMonthName = (monthNumber) => {
  const monthNames = [
    "Januari", "Februari", "Maret",
    "April", "Mei", "Juni",
    "Juli", "Agustus", "September",
    "Oktober", "November", "Desember"
  ];
  return monthNames[monthNumber];
};

// Get current date
const currentDate = new Date();

// Determine the year and month to display
const displayYear = currentDate.getMonth() === 0 ? currentDate.getFullYear() - 1 : currentDate.getFullYear();
const displayMonth = currentDate.getMonth() === 0 ? 12 : currentDate.getMonth() - 1;

const formattedDate = `${currentDate.getDate()} ${getMonthName(currentDate.getMonth())} ${currentDate.getFullYear()}`;

const GajiPdf = ({
  karyawan,
  gajiPokok,
  pinjaman,
  totalGaji,
  tunjanganMasaKerja,
  tunjanganJamMengajar,
  tunjanganMakan,
  tunjanganTransportasi,
  tunjanganKesehatan,
  tunjanganWalikelas,
  tunjanganJabatan,
  tunjanganPiket,
  tunjanganBimbinganSiang,
  tunjanganBimbinganMalam,
  tunjanganYayasan,
  penguranganTunjanganKetidakhadiran,
}) => (
  <Document>
    <Page style={styles.page}>
      <Text style={styles.title}>Slip Gaji</Text>

      <View style={styles.row}>
        <Text style={styles.label}>Nama Karyawan</Text>
        <Text style={styles.titikDua}>:</Text>
        <Text style={styles.value}>{karyawan?.nama}</Text>
      </View>

      <View style={styles.row}>
        <Text style={styles.label}>Jabatan</Text>
        <Text style={styles.titikDua}>:</Text>
        <Text style={styles.value}>
          {karyawan?.jabatan != "" ? karyawan?.jabatan.join(", ") : "-"}
        </Text>
      </View>

      <View style={styles.row}>
        <Text style={styles.label}>TMT</Text>
        <Text style={styles.titikDua}>:</Text>
        <Text style={styles.value}>
          {getMonthName(displayMonth)} {displayYear}
        </Text>
      </View>

      <View style={styles.table}>
        <View style={styles.tableRow}>
          <Text style={[styles.tableData, styles.tableHeader]}>
            Nama Tunjangan
          </Text>
          <Text style={[styles.tableData, styles.tableHeader]}>Hasil</Text>
        </View>
        {karyawan?.jabatan == "Pimpinan/Kepala Pesantren" ? (
          <View style={styles.tableRow}>
            <Text style={styles.tableJudul}>Gaji Pokok</Text>
            <Text style={styles.tableData}>
              Rp. {totalGaji.toLocaleString("id-ID")}
            </Text>
          </View>
        ) : (
          <>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Gaji Pokok</Text>
              <Text style={styles.tableData}>
                Rp. {gajiPokok.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Jabatan</Text>
              <Text style={styles.tableData}>
                Rp.{" "}
                {tunjanganJabatan != ""
                  ? tunjanganJabatan.toLocaleString("id-ID")
                  : 0}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Masa Kerja</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganMasaKerja.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Jam Mengajar</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganJamMengajar.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Transportasi</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganTransportasi.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Makan Siang</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganMakan.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Kesehatan</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganKesehatan.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Piket</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganPiket.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Bimbingan Siang</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganBimbinganSiang.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Bimbingan Malam</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganBimbinganMalam.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Walikelas</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganWalikelas.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Tunjangan Yayasan</Text>
              <Text style={styles.tableData}>
                Rp. {tunjanganYayasan.toLocaleString("id-ID")}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableJudul}>Potongan Gaji</Text>
              <Text style={styles.tableData}>
                Rp. {penguranganTunjanganKetidakhadiran.toLocaleString("id-ID")}
              </Text>
            </View>
          </>
        )}
        <View style={styles.tableRow}>
          <Text style={styles.tableJudul}>Pinjaman</Text>
          <Text style={styles.tableData}>
            Rp. {pinjaman.toLocaleString("id-ID")}
          </Text>
        </View>
        <View style={styles.tableRow}>
          <Text style={[styles.tableData, styles.tableHeader]}>Total Gaji</Text>
          <Text style={styles.tableData}>
            Rp. {totalGaji.toLocaleString("id-ID")}
          </Text>
        </View>
      </View>

      <View style={styles.centeredRow}>
  <Text style={styles.centeredLabel}>Diketahui:</Text>
  <Text style={styles.centeredValue}>Koto Perambahan, {formattedDate}</Text>
</View>

<View style={styles.centeredRow}>
  <Text style={styles.centeredLabel}>Ketua</Text>
  <Text style={styles.centeredValue}>Bendahara</Text>
</View>

<View style={styles.signatureContainer}>
  <Text style={styles.centeredLabel}>Harianto Arfit</Text>
  <Text style={styles.centeredValue}>Alhudry</Text>
</View>


    </Page>
  </Document>
);

export default GajiPdf;
